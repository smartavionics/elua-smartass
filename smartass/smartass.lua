
--[[

SmartASS Mk3 - Smart Air Speed Speaker

Copyright 2005-2020 Smart Avionics Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

--]]

----------------------------------------------------------------------------------
-- localise some system symbols we want quick access to

local tmr_delay = tmr.delay
local tmr_read = tmr.read
local tmr_getdiffnow = tmr.getdiffnow
local pio_pin_getval = pio.pin.getval
local dac = stm32f4.sa_dac
local adc_sample = adc.sample
local adc_getsample = adc.getsample
local band = bit.band
local lshift = bit.lshift
local rshift = bit.rshift
local HUGE = math.huge
local floor = math.floor
local abs = math.abs
local sqrt = math.sqrt
local format = string.format
local string_char = string.char
local string_byte = string.byte

----------------------------------------------------------------------------------
-- load required modules

local ad7798 = require "ad7798"
local lis3dh = require "lis3dh"
local wav = require "wav"

local read_pressure = ad7798.read_pressure
local read_acceleration = lis3dh.read

----------------------------------------------------------------------------------
-- constants

local SMARTASS_REVISION = "1.4"

local CAN_IF = 1

UC_DOWN_PIN = pio.PC_1
IN_1_PIN = pio.PC_2
IN_2_PIN = pio.PC_3

OUT_1_PIN = pio.PA_0
OUT_2_PIN = pio.PA_1

DIPSW_1_PIN = pio.PC_9
DIPSW_2_PIN = pio.PC_8
DIPSW_3_PIN = pio.PC_7
DIPSW_4_PIN = pio.PC_6

local PANEL_ADC_CHANNEL  = 10

local AVDD_UPDATE_PERIOD_US        = 60e6 -- once per minute
local TEMPERATURE_UPDATE_PERIOD_US = 10e6 -- once per ten seconds

local SPEED_UPDATE_PERIOD_US = 1e5 -- 1/10 second

local BASE_DIR = "/mmc/"

local CONF_DIR = BASE_DIR .. "conf/"
local PSCALING_FILE_NAME     = CONF_DIR .. "smartass.psc"
local POSS_FILE_NAME         = CONF_DIR .. "smartass.cal"
local CONFIG_FILE_NAME       = CONF_DIR .. "smartass.cnf"
local VOL_FILE_NAME          = CONF_DIR .. "smartass.vol"
local ZEROP_FILE_NAME        = CONF_DIR .. "smartass.zp"
local TSPEED_FILE_NAME       = CONF_DIR .. "smartass.ts"
local ACCEL_OFFSET_FILE_NAME = CONF_DIR .. "smartass.ao"
local ACCEL_COEF_FILE_NAME   = CONF_DIR .. "smartass.ac"

local AUDIO_FILES_DIR    = BASE_DIR .. "audio/"
local AUDIO_FILES_SUFFIX = ".wav"

local ALERT_PERIOD_MS = 2000

local SAVE_CONFIG_DELAY_MS = 10000 -- delay config saves for 10 seconds after last change
local SAVE_VOLUME_DELAY_MS = 10000

local ALERT_NONE       = 0
local ALERT_SUPPRESSED = 1
local ALERT_ACTIVE     = 2

local VERY_QUIET_VOLUME  = 30
local QUIET_VOLUME       = 40
local NORMAL_VOLUME      = 60
local LOUD_VOLUME        = 80
local VERY_LOUD_VOLUME   = 100

local VOLUME_SMOOTHING_ALPHA = 0.02

local SPEED_UNITS_KNOTS = 0
local SPEED_UNITS_MPH   = 1
local SPEED_UNITS_KPH   = 2

local KNOTS_FULL_SCALE = 248	        -- assuming r = 1.225 kg/M^3 and sensor is MPX2010
local PRESSURE_MV_FULL_SCALE = 1600   -- assuming +5V sensor supply and 128 gain
local PRESSURE_VREF_MV = 5000	        -- assuming Vref = sensor supply
local PRESSURE_ADC_COUNTS_MAX = 32768 -- 16 bit ADC in bipolar mode

local ADC_COUNTS_FULL_SCALE = (PRESSURE_MV_FULL_SCALE * PRESSURE_ADC_COUNTS_MAX / PRESSURE_VREF_MV)

local MPH_PER_KNOT = 1.151
local KPH_PER_KNOT = 1.852

-- speed is proportional to sqrt of pressure so we need to scale the sensor reading
-- such that the square root of the maximum reading is equal to KNOTS_FULL_SCALE

local PRESSURE_CONST_KNOTS = (KNOTS_FULL_SCALE * KNOTS_FULL_SCALE) / ADC_COUNTS_FULL_SCALE
local PRESSURE_CONST_MPH   = (PRESSURE_CONST_KNOTS * (MPH_PER_KNOT * MPH_PER_KNOT))
local PRESSURE_CONST_KPH   = (PRESSURE_CONST_KNOTS * (KPH_PER_KNOT * KPH_PER_KNOT))

local MAX_PRESSURE_DIFF_FOR_AUTO_ZERO = 25 -- in ADC counts (25 = approx 12 kts)

local MIN_BELIEVABLE_PRESSURE = 32000  -- if pressure is less than this, something must be broken

local MIN_TEMPERATURE_FOR_AUTO_ZERO = 0 -- in degrees C
local MAX_TEMPERATURE_FOR_AUTO_ZERO = 40 -- in degrees C

local AIRSPEED_REPEAT_PERIOD_MS = 3000

local MIN_SPOKEN_SPEED_KNOTS = 25
local MIN_SPOKEN_SPEED_MPH   = 30
local MIN_SPOKEN_SPEED_KPH   = 50

local SPEED_DIFFERENCE_THRESHOLD_KNOTS  =  5
local SPEED_DIFFERENCE_THRESHOLD_MPH    =  5
local SPEED_DIFFERENCE_THRESHOLD_KPH    = 10

local SPEED_DIRECTOR_QUIT_SLOW_MARGIN_PERCENT = 20
local SPEED_DIRECTOR_QUIT_FAST_MARGIN_PERCENT = 25

local SLIGHTLY_SLOW_MARGIN_PERCENT     = 5
local SLIGHTLY_SLOW_MARGIN_MIN_KNOTS   = 3
local SLIGHTLY_SLOW_MARGIN_MIN_MPH     = 4
local SLIGHTLY_SLOW_MARGIN_MIN_KPH     = 6

local VERY_SLOW_MARGIN_PERCENT     = 10
local VERY_SLOW_MARGIN_MIN_KNOTS   =  6
local VERY_SLOW_MARGIN_MIN_MPH     =  8
local VERY_SLOW_MARGIN_MIN_KPH     = 12

local INSANELY_SLOW_MARGIN_PERCENT     = 15
local INSANELY_SLOW_MARGIN_MIN_KNOTS   =  9
local INSANELY_SLOW_MARGIN_MIN_MPH     = 12
local INSANELY_SLOW_MARGIN_MIN_KPH     = 18

local SLIGHTLY_FAST_MARGIN_PERCENT   = 5
local SLIGHTLY_FAST_MARGIN_MIN_KNOTS = 3
local SLIGHTLY_FAST_MARGIN_MIN_MPH   = 4
local SLIGHTLY_FAST_MARGIN_MIN_KPH   = 6

local VERY_FAST_MARGIN_PERCENT     = 10
local VERY_FAST_MARGIN_MIN_KNOTS   =  6
local VERY_FAST_MARGIN_MIN_MPH     =  8
local VERY_FAST_MARGIN_MIN_KPH     = 12

local INSANELY_FAST_MARGIN_PERCENT    = 15
local INSANELY_FAST_MARGIN_MIN_KNOTS  =  9
local INSANELY_FAST_MARGIN_MIN_MPH    = 12
local INSANELY_FAST_MARGIN_MIN_KPH    = 18

local SPEED_DIRECTOR_HYSTERESIS_PERCENT = 2

-- as pulling small amounts of G is not particularly dangerous
-- but it does produce "slow" warnings we scale down the G
-- when it is small to reduce the effect it has - the mapping is:

-- when G is below G_MAP_LOW, clamp to 1
-- when G is in the range G_MAP_LOW to G_MAP_HIGH, map to between 1 and G_MAP_HIGH
-- when G is G_MAP_HIGH or more, use it unchanged

local G_MAP_LOW  = 1.1	-- approx 5% increase in stall speed
local G_MAP_HIGH = 2

----------------------------------------------------------------------------------
-- variables

local general_alert = ALERT_NONE
local uc_alert      = ALERT_NONE

local volume_fixed_percent = NORMAL_VOLUME
local smoothed_volume;

local speed_units

local pressure_const

local config_last_changed_at
local volume_last_changed_at

local button_pressed_at = nil
local button_pressed_duration = 0
local button_short_presses = 0
local last_button_short_press_at = 0

local last_speed_update_at = 0

local speed_director_volume
local speed_director_repeat_period
local one_g_stall_speed
local speed_director_min_speed
local speed_director_max_speed
local insanely_slow_speed
local very_slow_speed
local slightly_slow_speed
local insanely_fast_speed
local very_fast_speed
local slightly_fast_speed

local slightly_slow_hysteresis
local very_slow_hysteresis
local insanely_slow_hysteresis
local slightly_fast_hysteresis
local very_fast_hysteresis
local insanely_fast_hysteresis

local hysteresis

local gc_ms = 0

local fake_g;

local verbose = 0

local conf = {
  amplification = 1.5, -- compensate for quiet sound files
  accel_smoothing = 0.25, -- exponential smoothing factor
  speed_smoothing = 0.25, -- exponential smoothing factor
  repeat_period_multiplier = 2,
  auto_switch_mode = true,
  can_bitrate = 250000,
  can_aerospace_node_id = 1, -- nil to disable CAN tx
  sound_enabled = true,
  have_volume_switches = false,
  target_speed_locked = false,
  only_speak_when_slow = false,
  min_spoken_speed = 0,
}
smartass_conf = conf

local current = {
  speed = 0,		   -- knots, MPH or KPH
  pressure = 0,    -- ADC counts (temperature compensated)
  raw_pressure = 0, -- non-compensated pressure
  temperature = 0, --	C
  AVdd = 0,        -- V
  g = 0,			     -- G
  accel = { 0, 0, 0 }, -- mG
  -- the following are saved/restored to/from the SD card
  volume = 80,     -- percent (only saved/restored if conf.have_volume_switches is true)
  pressure_scaling = 1.0, -- span compensation
  pressure_offset_shift_slope = 3.7, -- ADC counts per deg C
  zero_pressure = 0,
  target_speed = 0,
  accel_offset = { 0, 0, 0 }, -- X, Y, Z (+ or - mG)
  accel_coef = { 0, 0, 0 }, -- X, Y, Z
}
smartass_current = current

----------------------------------------------------------------------------------
-- misc functions

function tprint(...)
  local now = tmr_read()
  io.write(format("%.06u.%06u: ", now / 1000000, now % 1000000))
  print(...)
end

function uc_down_asserted()
  return pio_pin_getval(UC_DOWN_PIN) == 0
end

function general_alert_input_asserted()
  return pio_pin_getval(IN_1_PIN) == 0
end

function spairs(t, order)
  -- collect the keys
  local keys = {}
  for k in pairs(t) do
    keys[#keys+1] = k
  end
  -- if order function given, sort by it by passing the table and keys a, b,
  -- otherwise just sort the keys
  if order then
    table.sort(keys, function(a,b) return order(t, a, b) end)
  else
    table.sort(keys)
  end
  -- return the iterator function
  local i = 0
  return function()
    i = i + 1
    if keys[i] then
      return keys[i], t[keys[i]]
    end
  end
end

function time(f, label)
  local before = tmr_read()
  f()
  local duration = tmr_getdiffnow(nil, before)
  if label then
    print(label .. ": " .. duration)
  end
  return duration
end

function delay_ms(ms)
  -- don't delay for longer than 10 mS at a time so that Lua interrupts get serviced regularly
  while ms > 10 do
    tmr_delay(nil, 10 * 1000)
    ms = ms - 10
  end
  tmr_delay(nil, ms * 1000)
end

function delay_s(s)
  delay_ms(s * 1000)
end

function print_banner()
  print("\nSmartASS-3 (Script Rev " .. SMARTASS_REVISION .. ", Firmware Rev " .. elua.version() .. ", " .. _VERSION .. ")\n")
end

--[[
function decimal_string(val, decimal_places)
  decimal_places = decimal_places or 2
  local sign = ""
  if val < 0 then
    sign = "-"
    val = -val
  end
  return format(sign .. "%d.%0" .. decimal_places .. "d", val / 10 ^ decimal_places, val % 10 ^ decimal_places)
end
--]]

function print_current_state()
  tprint(format("raw pressure = %d, pressure = %d (%d), speed = %.1f, temp = %.2f, AVdd = %.3f, mg = %d/%d/%d, g = %.2f, gc ms = %d, KB used = %d",
    floor(current.raw_pressure + 0.5), current.pressure, current.pressure - current.zero_pressure, current.speed, current.temperature,
    current.AVdd, current.accel[1], current.accel[2], current.accel[3], current.g, gc_ms, collectgarbage("count")))
end

----------------------------------------------------------------------------------
-- CAN functions

local can_message_code = 0

function can_aerospace_send(what, value)
  if conf.can_aerospace_node_id then
    -- increment can_message_code byte
    can_message_code = band(can_message_code + 1, 0xff)
    local result
    if what == "airspeed" then
      -- 315 = Indicated Airspeed
      local val = value * 32
      result = can.send(CAN_IF, 315, can.ID_STD,
        string_char(conf.can_aerospace_node_id, 6, 0, can_message_code, band(rshift(val, 8), 0xff), band(val, 0xff)))
    elseif what == "acceleration" then
      -- 302 = Body Normal Acceleration
      local val = value * 2048
      result = can.send(CAN_IF, 302, can.ID_STD,
        string_char(conf.can_aerospace_node_id, 6, 0, can_message_code, band(rshift(val, 8), 0xff), band(val, 0xff)))
    end
    --tprint("can_aerospace_send(" .. what .. ", " .. value .. ") = " .. tostring(result))
  end
end

----------------------------------------------------------------------------------
-- config save/restore functions

function save_config()
  local f, msg = io.open(CONFIG_FILE_NAME, "w+")
  if f then
    f:write("-- SmartASS config file\n")
    f:write("return {\n")
    for k, v in spairs(conf) do
      f:write("  ", k, " = ")
      if type(v) == "boolean" then
        f:write(tostring(v))
      elseif type(v) == "string" then
        f:write(format("%q", v))
      elseif type(v) == "nil" then
        f:write("nil")
      else
        f:write(format("%g", v))
      end
      f:write(",\n")
    end
    f:write("}\n")
    f:close()
    tprint("Configuration saved")
  else
    tprint("save_config(): " .. msg)
  end
end

function load_config()
  local cf, msg = loadfile(CONFIG_FILE_NAME)
  if cf then
    local status, val = pcall(cf)
    if status and type(val) == "table" then
      -- update existing members of conf, don't create new ones
      for k, v in pairs(conf) do
        if val[k] ~= nil then
          conf[k] = val[k]
        end
      end
    else
      print(val)
    end
  else
    tprint("load_config(): " .. msg)
  end
end

function save_current_value(file_name, value_name)
  local f, msg = io.open(file_name, "w+")
  if f then
    local v = current[value_name]
    if type(v) == "table" then
      -- only copes with simple arrays of numbers at the moment
      for i = 1, #v do
        f:write(format("%g ", v[i]))
      end
      f:write(format("\n"))
    else
      f:write(format("%g\n", v))
    end
    f:close()
    tprint(format("Saved current.%s", value_name))
  else
    tprint("save_current_value(" .. file_name .. ", " .. value_name .. "): " .. msg)
  end
end

function load_current_value(file_name, value_name)
  local f, msg = io.open(file_name, "r")
  if f then
    local val
    if type(current[value_name]) == "table" then
      -- only copes with simple arrays at the moment
      val = { }
      for i = 1, #current[value_name] do
        val[i] = f:read("*n")
      end
    else
      val = f:read("*n")
    end
    f:close()
    if val then
      current[value_name] = val
      tprint(format("Restored current.%s", value_name))
    else
      tprint("Failed to read value from " .. file_name)
    end
  else
    tprint("load_current_value(" .. file_name .. ", " .. value_name .. "): " .. msg)
  end
end

function save_zero_pressure()
  save_current_value(ZEROP_FILE_NAME, "zero_pressure")
end

function restore_zero_pressure()
  load_current_value(ZEROP_FILE_NAME, "zero_pressure")
end

function save_volume()
  save_current_value(VOL_FILE_NAME, "volume")
end

function restore_volume()
  load_current_value(VOL_FILE_NAME, "volume")
end

function save_pressure_offset_shift_slope()
  save_current_value(POSS_FILE_NAME, "pressure_offset_shift_slope")
end

function restore_pressure_offset_shift_slope()
  load_current_value(POSS_FILE_NAME, "pressure_offset_shift_slope")
end

function save_pressure_scaling()
  save_current_value(PSCALING_FILE_NAME, "pressure_scaling")
end

function restore_pressure_scaling()
  load_current_value(PSCALING_FILE_NAME, "pressure_scaling")
end

function save_target_speed()
  save_current_value(TSPEED_FILE_NAME, "target_speed")
end

function restore_target_speed()
  load_current_value(TSPEED_FILE_NAME, "target_speed")
end

function save_accel_offset()
  save_current_value(ACCEL_OFFSET_FILE_NAME, "accel_offset")
end

function restore_accel_offset()
  load_current_value(ACCEL_OFFSET_FILE_NAME, "accel_offset")
end

function save_accel_coef()
  save_current_value(ACCEL_COEF_FILE_NAME, "accel_coef")
end

function restore_accel_coef()
  load_current_value(ACCEL_COEF_FILE_NAME, "accel_coef")
end

----------------------------------------------------------------------------------
-- panel functions

local function poll_panel()
  adc_sample(PANEL_ADC_CHANNEL, 1)
  local val = adc_getsample(PANEL_ADC_CHANNEL)
  if not val then
    return true
  end
  local button_released = false
  if conf.have_volume_switches then
    if val < 1000 then
      -- push switch is pressed
      if button_pressed_at then
        button_pressed_duration = button_pressed_duration + tmr_getdiffnow(nil, button_pressed_at)
      end
      button_pressed_at = tmr_read()
    elseif button_pressed_at then
      button_released = true
    elseif val < 2000 then
      if current.volume > 0  then
        current.volume = current.volume - 1
        dac.setscaling(volume_fixed_percent * current.volume / 100)
        volume_last_changed_at = tmr_read()
        --print("volume now " .. current.volume)
      end
    elseif val < 3000 then
      if current.volume < 100  then
        current.volume = current.volume + 1
        dac.setscaling(volume_fixed_percent * current.volume / 100)
        volume_last_changed_at = tmr_read()
        --print("volume now " .. current.volume)
      end
    end
  else
    -- volume is set by potentiometer (10K linear in series with 1K, low resistance = high volume)
    if val < 100 then
      -- push switch is pressed
      if button_pressed_at then
        button_pressed_duration = button_pressed_duration + tmr_getdiffnow(nil, button_pressed_at)
      end
      button_pressed_at = tmr_read()
    elseif button_pressed_at then
      button_released = true
    elseif val > 4000 then
      -- failed open circuit?
      if current.volume ~= 100 then
        current.volume = 100
        print("Volume control disconnected?")
      end
    else
      local new_volume = nil
      -- ignore values (< 2048) that could be due to switch opening/closing
      if val >= 2048 then
        new_volume = 100 - floor((val - 2048) * 0.058 + 0.5)
        if new_volume < 0 then
          new_volume = 0
        end
        if smoothed_volume == nil then
          smoothed_volume = new_volume
        else
          smoothed_volume = smoothed_volume - smoothed_volume * VOLUME_SMOOTHING_ALPHA
          smoothed_volume = smoothed_volume + new_volume * VOLUME_SMOOTHING_ALPHA
          new_volume = floor(smoothed_volume + 0.5)
        end
      end
      if new_volume and abs(current.volume - new_volume) > 1 then
        current.volume = new_volume
        dac.setscaling(volume_fixed_percent * current.volume / 100)
        print("volume now " .. current.volume)
      end
    end
  end
  if button_released then
    button_pressed_duration = button_pressed_duration + tmr_getdiffnow(nil, button_pressed_at)
    if button_pressed_duration > 50000 and button_pressed_duration < 1000000 then
      button_short_presses = button_short_presses + 1
      last_button_short_press_at = tmr_read()
    end
    button_pressed_at = nil
  end
  return true
end

----------------------------------------------------------------------------------
-- sound functions

function disable_dac()
  -- if we can, disable DAC output to reduce noise
  if dac.enableoutput then
    dac.enableoutput(0, false)
  end
end

function gen_noise(duration)
  dac.setup(0)
  local rng_read = stm32f4.cpu.rng.read
  local start = tmr_read()
  while(tmr_getdiffnow(nil, start) < duration * 1000000) do
    dac.putsample(0, band(rng_read(), 0xff) * current.volume / 100)
  end
end

function play_sound(name, volume_percent)
  if conf.sound_enabled then
    if type(name) == "table" then
      local result = true
      for i = 1, #name do
        if not play_sound(name[i], volume_percent) then
          result = false
        end
      end
      return result
    end
    if volume_percent then
      volume_fixed_percent = conf.amplification * volume_percent;
      if volume_percent > NORMAL_VOLUME and current.volume < 40 then
        -- volume is turned down but this could be an important message so turn the volume up
        current.volume = current.volume * 2
      end
    else
      volume_fixed_percent = conf.amplification * NORMAL_VOLUME;
    end
    local callback = function()
      if tmr_getdiffnow(nil, last_speed_update_at) > SPEED_UPDATE_PERIOD_US then
        update_speed_and_g()
        if verbose > 0 then
          print_current_state()
        end
      end
      return poll_panel()
    end
    local wav_file_name = AUDIO_FILES_DIR .. name .. AUDIO_FILES_SUFFIX
    --tprint("Playing " .. wav_file_name)
    local result, msg = pcall(wav.playwavfile, wav_file_name, volume_fixed_percent * current.volume / 100, 0, callback)
    if not result then
      tprint(msg)
    end
    --tprint("Playing " .. wav_file_name .. " done")
    return result
  end
  return nil
end

function say_digit(val)
  play_sound(string_char(string_byte("0") + val))
end

function say_number(val)
  val = floor(val)
  local hundreds = floor(val / 100)
  local tens_and_units = val % 100

  if val == 0 then
    play_sound("0")
  elseif hundreds ~= 0 then
    say_digit(hundreds)
    if tens_and_units < 10 then
      play_sound("hundred")
    end
    if tens_and_units ~= 0 then
      if tens_and_units < 10 then
        play_sound("and")
      end
      say_number(tens_and_units)
    end
  else
    local tens = floor(tens_and_units / 10)
    local units = tens_and_units % 10
    if tens == 1 then
      play_sound(string_char(string_byte("1"), string_byte("0") + units))
    else
      if tens > 0 then
        play_sound(string_char(string_byte("0") + tens, string_byte("0")))
      end
      if units ~= 0 then
        say_digit(units)
      end
    end
  end
end

function croak(fail_code)
  print(format("\n*** croak(%d) ***", fail_code))
  while true do
    if play_sound({"chime", "fail"}) then
      say_number(fail_code)
    else
      gen_noise(0.5)
    end
    current.AVdd = ad7798.read_AVdd()
    update_temperature()
    read_pressure_and_acceleration()
    print_current_state()
    delay_s(2)
  end
end

----------------------------------------------------------------------------------
-- temperature functions

function update_temperature(num_samples)
  num_samples = num_samples or 4
  -- assumes LM19 temperature sensor
  local TEMPERATURE_ADC_COUNTS_MAX = 65535
  local TEMPERATURE_OFFSET_uV      = 1858000
  local TEMPERATURE_uV_PER_DEGREE_C = 11700
  local TEMPERATURE_OFFSET = TEMPERATURE_OFFSET_uV / TEMPERATURE_uV_PER_DEGREE_C
  local tv = 0
  for i = 1, num_samples do
    tv = tv + ad7798.read_temperature()
  end
  current.temperature = -((tv * current.AVdd / TEMPERATURE_ADC_COUNTS_MAX * 1000000 / (TEMPERATURE_uV_PER_DEGREE_C * num_samples)) - TEMPERATURE_OFFSET)
  current.temperature = floor(current.temperature * 100 + 0.5) / 100
end

----------------------------------------------------------------------------------
-- pressure functions

function read_pressure_and_acceleration(num_samples)

  num_samples = num_samples or 1

  local accel_x = 0
  local accel_y = 0
  local accel_z = 0

  if num_samples == 1 then
    current.raw_pressure = read_pressure()
    local a = read_acceleration()
    accel_x = floor(a[1] + 0.5)
    accel_y = floor(a[2] + 0.5)
    accel_z = floor(a[3] + 0.5)
  else
    local p = 0
    for i = 1, num_samples do
      p = p + read_pressure()
      local a = read_acceleration()
      accel_x = accel_x + a[1]
      accel_y = accel_y + a[2]
      accel_z = accel_z + a[3]
    end
    current.raw_pressure = p / num_samples
    accel_x = floor(accel_x / num_samples + 0.5)
    accel_y = floor(accel_y / num_samples + 0.5)
    accel_z = floor(accel_z / num_samples + 0.5)
  end

  current.pressure = floor(current.raw_pressure + (25 - current.temperature) * current.pressure_offset_shift_slope + 0.5)

  -- add in offsets
  current.accel[1] = accel_x + current.accel_offset[1]
  current.accel[2] = accel_y + current.accel_offset[2]
  current.accel[3] = accel_z + current.accel_offset[3]
end

function record_zero_pressure()
  -- read the temperature so that the pressure offset drift can be calculated
  update_temperature()
  read_pressure_and_acceleration(32)
  if current.zero_pressure ~= current.pressure then
    current.zero_pressure = current.pressure
    save_zero_pressure()
  end
end

function calibrate()
  local sum_of_slopes = 0
  local num_samples = 0
  local last_pressure = 0
  local last_temperature = 0
  local max_temperature = 0
  local finished = false

  print("")
  tprint("Calibrating pressure offset shift slope (hit q to quit)\n")

  current.AVdd = ad7798.read_AVdd()
  update_temperature()

  while not finished do
    if uart.getchar(uart.CDC, 0) == "q" then
      print("Quit")
      return
    end
    update_temperature()
    -- oversample pressure to get a couple of extra bits of precision
    read_pressure_and_acceleration(32)
    current.pressure = nil -- bug catcher!
    current.raw_pressure = floor(current.raw_pressure * 4 + 0.5) / 4
    if current.temperature > max_temperature then
      max_temperature = current.temperature
    end
    if current.temperature >= (max_temperature - 0.5) then
      tprint(format("T = %.2f, P = %.2f", current.temperature, current.raw_pressure))
      last_temperature = current.temperature
      delay_s(15)
    elseif last_pressure == 0 then
      tprint(format("T = %.2f, P = %.2f - starting averaging", current.temperature, current.raw_pressure))
      last_pressure = current.raw_pressure
      last_temperature = current.temperature
      delay_s(60)
    else
      local delta_pressure = current.raw_pressure - last_pressure
      local delta_temperature = current.temperature - last_temperature
      if delta_temperature >= -0.5 then
        finished = true
      else
        local slope = delta_pressure / delta_temperature
        sum_of_slopes = sum_of_slopes + slope
        num_samples = num_samples + 1
        tprint(format("[%d] T = %.2f, dT = %.2f, P = %.2f, dP = %.2f, dP/dT = %.2f, avg dP/dT = %.2f",
          num_samples,
          current.temperature,
          delta_temperature,
          current.raw_pressure,
          delta_pressure,
          slope,
          sum_of_slopes / num_samples))
        if slope <= 0 then
          tprint("Slope not +ve, restarting averaging...")
          sum_of_slopes = 0
          num_samples = 0
        end
        last_pressure = current.raw_pressure
        last_temperature = current.temperature
        delay_s(60)
      end
    end
  end

  tprint("Finished averaging")

  if num_samples > 0 then
    current.pressure_offset_shift_slope = floor(100 * sum_of_slopes / num_samples + 0.5) / 100
    save_pressure_offset_shift_slope()
    -- save current compensated pressure as zero pressure
    read_pressure_and_acceleration(32)
    record_zero_pressure()
  end
end

function set_pressure_const()
  if speed_units == SPEED_UNITS_MPH then
    pressure_const = PRESSURE_CONST_MPH
  elseif speed_units == SPEED_UNITS_KPH then
    pressure_const = PRESSURE_CONST_KPH
  else
    pressure_const = PRESSURE_CONST_KNOTS
  end
  pressure_const = pressure_const * current.pressure_scaling
end

----------------------------------------------------------------------------------
-- speed and acceleration functions

function set_speed_thresholds()

  one_g_stall_speed = floor(current.target_speed * 0.77)

  speed_director_min_speed = floor(current.target_speed * (100 - SPEED_DIRECTOR_QUIT_SLOW_MARGIN_PERCENT) / 100)
  speed_director_max_speed = floor(current.target_speed * (100 + SPEED_DIRECTOR_QUIT_FAST_MARGIN_PERCENT) / 100)

  if speed_units == SPEED_UNITS_KPH then
    insanely_slow_speed = current.target_speed - INSANELY_SLOW_MARGIN_MIN_KPH
    very_slow_speed     = current.target_speed - VERY_SLOW_MARGIN_MIN_KPH
    slightly_slow_speed = current.target_speed - SLIGHTLY_SLOW_MARGIN_MIN_KPH
    insanely_fast_speed = current.target_speed + INSANELY_FAST_MARGIN_MIN_KPH
    very_fast_speed     = current.target_speed + VERY_FAST_MARGIN_MIN_KPH
    slightly_fast_speed = current.target_speed + SLIGHTLY_FAST_MARGIN_MIN_KPH
  elseif speed_units == SPEED_UNITS_MPH then
    insanely_slow_speed = current.target_speed - INSANELY_SLOW_MARGIN_MIN_MPH
    very_slow_speed     = current.target_speed - VERY_SLOW_MARGIN_MIN_MPH
    slightly_slow_speed = current.target_speed - SLIGHTLY_SLOW_MARGIN_MIN_MPH
    insanely_fast_speed = current.target_speed + INSANELY_FAST_MARGIN_MIN_MPH
    very_fast_speed     = current.target_speed + VERY_FAST_MARGIN_MIN_MPH
    slightly_fast_speed = current.target_speed + SLIGHTLY_FAST_MARGIN_MIN_MPH
  else
    insanely_slow_speed = current.target_speed - INSANELY_SLOW_MARGIN_MIN_KNOTS
    very_slow_speed     = current.target_speed - VERY_SLOW_MARGIN_MIN_KNOTS
    slightly_slow_speed = current.target_speed - SLIGHTLY_SLOW_MARGIN_MIN_KNOTS
    insanely_fast_speed = current.target_speed + INSANELY_FAST_MARGIN_MIN_KNOTS
    very_fast_speed     = current.target_speed + VERY_FAST_MARGIN_MIN_KNOTS
    slightly_fast_speed = current.target_speed + SLIGHTLY_FAST_MARGIN_MIN_KNOTS
  end

  -- all round to nearest
  local s;
  s = floor((current.target_speed * (100 - INSANELY_SLOW_MARGIN_PERCENT) + 50) / 100)
  if s < insanely_slow_speed then
    insanely_slow_speed = s
  end

  s = floor((current.target_speed * (100 - VERY_SLOW_MARGIN_PERCENT) + 50) / 100)
  if s < very_slow_speed then
    very_slow_speed = s
  end

  s = floor((current.target_speed * (100 - SLIGHTLY_SLOW_MARGIN_PERCENT) + 50) / 100)
  if s < slightly_slow_speed then
    slightly_slow_speed = s
  end

  s = floor((current.target_speed * (100 + INSANELY_FAST_MARGIN_PERCENT) + 50) / 100)
  if s > insanely_fast_speed then
    insanely_fast_speed = s
  end

  s = floor((current.target_speed * (100 + VERY_FAST_MARGIN_PERCENT) + 50) / 100)
  if s > very_fast_speed then
    very_fast_speed = s
  end

  s = floor((current.target_speed * (100 + SLIGHTLY_FAST_MARGIN_PERCENT) + 50) / 100)
  if s > slightly_fast_speed then
    slightly_fast_speed = s
  end

  hysteresis = floor((current.target_speed * SPEED_DIRECTOR_HYSTERESIS_PERCENT + 99) / 100) -- round up
  if hysteresis < 1 then
    hysteresis = 1
  end
end

function entering_speed_director_mode(verbose)
  speed_director_volume = NORMAL_VOLUME
  if verbose then
    play_sound("tsi")
    say_number(current.target_speed)
    disable_dac()
  end
  slightly_slow_hysteresis = 0
  very_slow_hysteresis = 0
  insanely_slow_hysteresis = 0
  slightly_fast_hysteresis = 0
  very_fast_hysteresis = 0
  insanely_fast_hysteresis = 0
end

function remember_orientation()
  local len = sqrt((current.accel[1] * current.accel[1]) +
                   (current.accel[2] * current.accel[2]) +
                   (current.accel[3] * current.accel[3]))
  current.accel_coef = {
    floor(current.accel[1] * 1000 / len + 0.5),
    floor(current.accel[2] * 1000 / len + 0.5),
    floor(current.accel[3] * 1000 / len + 0.5)
  }
  save_accel_coef()
end

function acceleration_zeroed()
  return current.accel_offset[1] ~= 0 or current.accel_offset[2] ~= 0 or current.accel_offset[3] ~= 0
end

function unit_orientated()
  return current.accel_coef[1] ~= 0 or current.accel_coef[2] ~= 0 or current.accel_coef[3] ~= 0
end

function save_acceleration_offsets()
  current.accel_offset = { 0, 0, 0 }
  read_pressure_and_acceleration(32)
  if current.accel[1] ~= 0 then
    current.accel_offset[1] = floor(-current.accel[1] + 0.5)
  end
  if current.accel[2] ~= 0 then
    current.accel_offset[2] = floor(-current.accel[2] + 0.5)
  end
  if current.accel[3] ~= 1000 then
    current.accel_offset[3] = floor(1000 - current.accel[3] + 0.5)
  end
  save_accel_offset()
end

function update_speed_and_g()

  read_pressure_and_acceleration()

  local dp = current.pressure - current.zero_pressure
  if dp < 0 then
    dp = 0
  end

  local speed = sqrt(dp * pressure_const)

  if speed >= 1 then
    current.speed = current.speed * (1 - conf.speed_smoothing) + speed * conf.speed_smoothing
  else
    current.speed = 0
  end

  if unit_orientated() then
    -- unit is orientated so g can be calculated
    local g = (current.accel[1] * current.accel_coef[1] +
               current.accel[2] * current.accel_coef[2] +
               current.accel[3] * current.accel_coef[3]) / 1000000
    if fake_g then
      g = g + fake_g
      fake_g = fake_g > 0.01 and fake_g * 0.5
    end
    current.g = current.g - current.g * conf.accel_smoothing + g * conf.accel_smoothing
  end

  last_speed_update_at = tmr_read()

end

----------------------------------------------------------------------------------
-- main

function smartass (callback)

  -- delay for a few seconds so that we have time to fire up the USB serial link
  delay_s(3)

  print_banner()

  lis3dh.init()
  ad7798.init()

  do
    -- ensure config directory exists by trying to access main config file
    local conf_file = io.open(CONFIG_FILE_NAME, "r")
    if conf_file then
      conf_file:close()
    else
      -- couldn't access the config file so create the directory using the elua shell (ugh)
      elua.shell("mkdir " .. string.sub(CONF_DIR, 1, -2)) -- sans trailing /
      -- save the config file so we don't try to create directory again
      save_config()
    end
  end

  load_config()

  pio.pin.setpull(pio.PULLUP, DIPSW_1_PIN, DIPSW_2_PIN, DIPSW_3_PIN, DIPSW_4_PIN)
  pio.pin.setdir(pio.OUTPUT, OUT_1_PIN, OUT_2_PIN)
  pio.pin.setval(0, OUT_1_PIN, OUT_2_PIN)

  local dip_sw1, dip_sw2, dip_sw3, dip_sw4 = pio.pin.getval(DIPSW_1_PIN, DIPSW_2_PIN, DIPSW_3_PIN, DIPSW_4_PIN)

  if dip_sw1 == 0 then
    speed_units = SPEED_UNITS_MPH
  elseif dip_sw2 == 0 then
    speed_units = SPEED_UNITS_KPH
  else
    speed_units = SPEED_UNITS_KNOTS
  end

  do
    local speed_unit_names = { [SPEED_UNITS_KNOTS] = "Knots", [SPEED_UNITS_MPH] = "MPH", [SPEED_UNITS_KPH] = "KPH" }
    tprint("Speeds are in " .. speed_unit_names[speed_units])
  end
  local undercarriage_alert_enabled = dip_sw3 == 0
  tprint("Undercarriage alert is " .. (undercarriage_alert_enabled and "enabled" or "disabled"))
  local general_alert_enabled = dip_sw4 == 0
  tprint("General alert is " .. (general_alert_enabled and "enabled" or "disabled"))

  can.setup(CAN_IF, conf.can_bitrate)

  local last_spoken_speed = 0
  local speed_director_mode = false
  local speed_muted = false
  local at_cruise_speed = false
  local last_report_at
  local last_alert_at
  local sd_repeat_period = 0 -- also used to track the current SD band
  local early_pressure

  current.AVdd = ad7798.read_AVdd()
  update_temperature()

  if conf.have_volume_switches then
    restore_volume()
  end
  restore_zero_pressure()
  restore_pressure_offset_shift_slope()
  restore_pressure_scaling()
  restore_target_speed()
  restore_accel_offset()
  restore_accel_coef()

  set_speed_thresholds()
  set_pressure_const()

  do
    local speed_unit_sounds = { [SPEED_UNITS_KNOTS] = "asik", [SPEED_UNITS_MPH] = "asimph", [SPEED_UNITS_KPH] = "asikph" }
    play_sound(speed_unit_sounds[speed_units])
    disable_dac()
    delay_s(1)
  end

  read_pressure_and_acceleration(32)
  poll_panel()
  if button_pressed_at or
    (current.temperature >= MIN_TEMPERATURE_FOR_AUTO_ZERO and
     current.temperature <= MAX_TEMPERATURE_FOR_AUTO_ZERO) then
    local zdiff = abs(current.pressure - current.zero_pressure)
    if button_pressed_at or current.zero_pressure == 0 then
      if zdiff > 1 then
        record_zero_pressure()
      end
      play_sound({"airspeed", "zeroed"})
    elseif zdiff < MAX_PRESSURE_DIFF_FOR_AUTO_ZERO then
      -- remember the current pressure
      early_pressure = current.pressure
      -- say "zeroed" here even though the actual zeroing won't happen for another 30 seconds
      play_sound({"airspeed", "zeroed"})
    else
      -- pressure diff is too high (aircraft moving?) so don't
      -- auto-zero and tell pilot to check against ASI
      last_spoken_speed = 1
      tprint("Pressure difference too great to auto-zero")
      play_sound({"chime", "airspeed", "not", "zeroed"})
    end
    disable_dac()
  end

  -- croak if temperature very low or high
  if current.temperature < -50 or
     current.temperature > 100 then
    croak(1)
  end

  -- croak if pressure is very low
  if current.pressure < MIN_BELIEVABLE_PRESSURE then
    croak(2)
  end

  -- croak if any axis is outside of +/- 3G
  if abs(current.accel[1]) > 3000 then
    croak(3)
  end
  if abs(current.accel[2]) > 3000 then
    croak(4)
  end
  if abs(current.accel[3]) > 3000 then
    croak(5)
  end

  if not acceleration_zeroed() then
    print("\n*** G not zeroed ***\n")
    --play_sound({"chime", "g", "not", "zeroed"})
  end

  -- warn if unit not orientated
  if not unit_orientated() then
    play_sound({"g", "disabled"})
    disable_dac()
  end

  local AVdd_last_updated_at = tmr_read()
  local temperature_last_updated_at = AVdd_last_updated_at
  local running = true

  gc_ms = floor(time(function () collectgarbage("collect") end) / 1000)

  update_speed_and_g()
  print_current_state()

  -- initialise key command map
  local cmap = {}
  cmap.C = function()
    io.write("Enter calibration value: ");
    local val = tonumber(io.read())
    if val then
      current.pressure_offset_shift_slope = val
      save_pressure_offset_shift_slope()
    else
      print("Number expected")
    end
  end
  cmap.K = function()
    calibrate()
  end
  cmap.E = function ()
    io.write("Enter lua statement: ");
    local line = io.read()
    if line then
      local chunk, msg = loadstring(line)
      if chunk then
        local result, rest = pcall(chunk)
        print(rest)
      else
        print(msg)
      end
    end
  end
  cmap.i = function()
    print_banner()
    print(format("MCU = %s, SYSCLK = %d MHz", pd.cpu(), stm32f4.cpu.getclocksfreq().SYSCLK / 1000000))
    print(format("MCU ID = %08X", stm32f4.cpu.read_mcu_device_id()))
    do
      local udi = stm32f4.cpu.read_unique_device_id()
      print(format("UNIQUE ID = %08X-%08X-%08X", udi[3], udi[2], udi[1]))
    end
    print("")
    if true then
      for k, v in spairs(current) do
        if type(v) == "table" then
          local val = ""
          for i = 1, #v do
            val = val .. " " .. tostring(v[i])
          end
          print("current." .. k .. " = {" .. val .. " }")
        else
          print("current." .. k .. " = " .. tostring(v))
        end
      end
    end
  end
  cmap.c = function()
    print("")
    for k, v in spairs(conf) do
      print("conf." .. k .. " = " .. tostring(v))
    end
  end
  cmap.t = function()
    print("")
    print("target speed        = " .. current.target_speed)
    print("1 G stall speed     = " .. one_g_stall_speed)
    print("insanely slow speed = " .. insanely_slow_speed)
    print("very slow speed     = " .. very_slow_speed)
    print("slightly slow speed = " .. slightly_slow_speed)
    print("slightly fast speed = " .. slightly_fast_speed)
    print("very fast speed     = " .. very_fast_speed)
    print("insanely fast speed = " .. insanely_fast_speed)
    print("hysteresis          = " .. hysteresis)
    print("")
  end
  cmap.g = function()
    fake_g = fake_g and fake_g + 1 or 1
  end
  cmap.G = function()
    io.write("Level unit and then hit enter: ")
    io.read()
    save_acceleration_offsets()
  end
  cmap.Q = function()
    running = false
  end
  cmap.s = function()
    save_config()
    config_last_changed_at = nil
  end
  cmap.S = function()
    io.write("Enter pressure scaling value: ");
    local val = tonumber(io.read())
    if val and val ~= current.pressure_scaling then
      current.pressure_scaling = val
      save_pressure_scaling()
      set_pressure_const()
    else
      print("Number expected")
    end
  end
  cmap.v = function()
    verbose = 1
  end
  cmap.V = function()
    verbose = verbose > 0 and 0 or 2
  end
  cmap.z = function()
    record_zero_pressure()
    play_sound({"airspeed", "zeroed"})
    disable_dac()
  end
  cmap.R = function()
    stm32f4.cpu.reset()
  end
  cmap["#"] = function ()
    io.write("Enter shell command: ");
    local line = io.read()
    if line then
      elua.shell(line)
    end
  end
  cmap.h = function()
    print_banner()
    print("Commands:")
    print("  c = list config values")
    print("  i = print some info (including current values)")
    print("  t = print threshold speeds")
    print("  v = print diagnostic line")
    print("  V = toggle continuous diagnostics")
    print("  z = zero airspeed")
    print("  # cmd = execute Lua shell command 'cmd'")
    print("\nGuru commands:")
    print("  C val = set temperature calibration value to 'val'")
    print("  K = calibrate temperature compensation")
    print("  E stat = evaluate Lua statement 'stat'")
    print("  g = generate transient +ve G")
    print("  G = zero acceleration")
    print("  Q = quit to shell")
    print("  R = reset MCU")
    print("  s = save config")
    print("  S val = set pressure scaling value to 'val'")
  end
  cmap.H = cmap.h
  for i, v in ipairs({ "", "\r", " " }) do
    cmap[v] = function()
      -- do nothing
    end
  end

  while running do

    if callback then
      callback(current.speed, current.g)
    end

    poll_panel()

    do
      -- read char from UART and map into command function
      local c = uart.getchar(uart.CDC, 0);
      (cmap[c] or cmap.h)(c)
    end

    if config_last_changed_at and tmr_getdiffnow(nil, config_last_changed_at) > SAVE_CONFIG_DELAY_MS * 1000 then
      save_config()
      config_last_changed_at = nil
    end

    if volume_last_changed_at and tmr_getdiffnow(nil, volume_last_changed_at) > SAVE_VOLUME_DELAY_MS * 1000 then
      save_volume()
      volume_last_changed_at = nil
    end

    -- explicitly request garbage collection to happen now
    gc_ms = floor(time(function () collectgarbage("collect") end) / 1000)
    if(gc_ms > 10) then
      tprint("gc took " .. gc_ms)
    end

    if tmr_getdiffnow(nil, AVdd_last_updated_at) > AVDD_UPDATE_PERIOD_US then
      current.AVdd = ad7798.read_AVdd()
      AVdd_last_updated_at = tmr_read()
      last_speed_update_at = 0 -- force speed update
    elseif tmr_getdiffnow(nil, temperature_last_updated_at) > TEMPERATURE_UPDATE_PERIOD_US then
      update_temperature()
      temperature_last_updated_at = tmr_read()
      last_speed_update_at = 0 -- force speed update
    end

    if tmr_getdiffnow(nil, last_speed_update_at) > SPEED_UPDATE_PERIOD_US then
      update_speed_and_g()
      if verbose > 0 then
        print_current_state()
        verbose = verbose > 1 and verbose or 0
      end
      if speed_units == SPEED_UNITS_MPH then
        can_aerospace_send("airspeed", floor(current.speed / MPH_PER_KNOT + 0.5))
      elseif speed_units == SPEED_UNITS_KPH then
        can_aerospace_send("airspeed", floor(current.speed / KPH_PER_KNOT + 0.5))
      else
        can_aerospace_send("airspeed", floor(current.speed + 0.5))
      end
      if unit_orientated() then
        can_aerospace_send("acceleration", current.g)
      end
    end

    -- Due to self-heating, the pressure sensor zero value will change (a little) from the
    -- the cold value so after running for 30 seconds we check if the current pressure is
    -- still OK to use for zero and, if so, use it. Otherwise, we use the pressure remembered
    -- on startup for zero.
    if early_pressure and tmr_read() >= 30 * 1000000 then
      read_pressure_and_acceleration(32)
      local zdiff = abs(current.pressure - current.zero_pressure)
      if zdiff < MAX_PRESSURE_DIFF_FOR_AUTO_ZERO then
        tprint(format("Using current pressure for zero (%d)", current.pressure))
        if zdiff > 1 then
          current.zero_pressure = current.pressure
          save_zero_pressure()
          update_speed_and_g()
        else
          tprint("Zero pressure unchanged")
        end
      else
        tprint(format("Using early pressure for zero (%d)", early_pressure))
        if abs(early_pressure - current.zero_pressure) > 1 then
          current.zero_pressure = early_pressure
          save_zero_pressure()
          update_speed_and_g()
        else
          tprint("Zero pressure unchanged")
        end
      end
      early_pressure = nil
    end

    local min_spoken_speed = MIN_SPOKEN_SPEED_KNOTS
    local speed_diff_threshold = SPEED_DIFFERENCE_THRESHOLD_KNOTS

    if speed_units == SPEED_UNITS_MPH then
      speed_diff_threshold = SPEED_DIFFERENCE_THRESHOLD_MPH
      min_spoken_speed = MIN_SPOKEN_SPEED_MPH
    elseif speed_units == SPEED_UNITS_KPH then
      speed_diff_threshold = SPEED_DIFFERENCE_THRESHOLD_KPH
      min_spoken_speed = MIN_SPOKEN_SPEED_KPH
    end

    if conf.min_spoken_speed > 0 then
      min_spoken_speed = conf.min_spoken_speed
    end

    -- see if button has been pressed
    local beeps = 0;
    if button_pressed_duration > 1000000 then
      -- switch may have been pressed while a long audio message was
      -- being output so limit the duration to be less than two beeps!
      button_pressed_duration = 1000000
    end
    while button_pressed_at do
      local escms = button_pressed_duration / 1000;
      if escms >= (beeps * 3000 + 500) then
        play_sound("beep")
        disable_dac()
        beeps = beeps + 1
        if beeps > 1 then
          delay_ms(100)
          say_number(beeps)
          disable_dac()
        end
      end
      poll_panel()
    end
    button_pressed_duration = 0

    if beeps > 0 then
      -- long presses take precedence
      button_short_presses = 0
    end

    if beeps == 6 and conf.only_speak_when_slow ~= nil then
      conf.only_speak_when_slow = not conf.only_speak_when_slow
      save_config()
      say_number(beeps);
      if conf.only_speak_when_slow then
        play_sound("enabled")
      else
        play_sound("disabled")
      end
      disable_dac()
    elseif beeps == 5 and conf.target_speed_locked ~= nil then
      conf.target_speed_locked = not conf.target_speed_locked
      save_config()
      say_number(beeps);
      if conf.target_speed_locked then
        play_sound("enabled")
      else
        play_sound("disabled")
      end
      disable_dac()
    elseif beeps == 4 then
      -- four beeps = zero pressure
      record_zero_pressure()
      play_sound({"airspeed", "zeroed"})
      disable_dac()
    elseif beeps == 3 then
      -- three beeps = capture the unit's orientation
      read_pressure_and_acceleration(4)	-- only acceleration used
      remember_orientation()
      play_sound({"orientat", "set"})
      disable_dac()
    elseif beeps == 2 then
      -- two beeps = cycle airspeed repeat period (x1, x2, x4)
      conf.repeat_period_multiplier = conf.repeat_period_multiplier * 2
      if conf.repeat_period_multiplier >= 8 then
        conf.repeat_period_multiplier = 1
      end
      save_config()
      play_sound("rpi")
      disable_dac()
      say_number(conf.repeat_period_multiplier * AIRSPEED_REPEAT_PERIOD_MS / 1000)
      disable_dac()
    elseif beeps == 1 then
      -- long press = capture current speed
      if current.speed >= min_spoken_speed then
        -- set speed director mode if speed high enough to be spoken
        speed_director_mode = true
        speed_muted = false
        at_cruise_speed = false
        if current.target_speed == 0 or not conf.target_speed_locked then
          current.target_speed = floor(current.speed + 0.5)
          -- save target speed for future
          save_target_speed()
        end
        set_speed_thresholds()
        uc_alert = ALERT_NONE -- arm the UC alert
        last_report_at = nil
        speed_director_repeat_period = 0
        entering_speed_director_mode(true)
      end
    end
    button_pressed_duration = 0

    if button_short_presses > 0 and tmr_getdiffnow(nil, last_button_short_press_at) > 500000 then
      if general_alert == ALERT_ACTIVE then
        general_alert = ALERT_SUPPRESSED
        -- play_sound("alertcan")
      elseif uc_alert == ALERT_ACTIVE then
        uc_alert = ALERT_SUPPRESSED
        -- play_sound("alertcan")
      elseif button_short_presses >= 2 then
        -- 2 presses -> mute
        if current.speed >= min_spoken_speed then
          speed_muted = true
          speed_director_mode = false
          play_sound("goodbye")
          disable_dac()
        end
      else
        -- 1 short press changes between talking ASI, speed director
        -- and mute depending on the current speed
        if speed_director_mode then
          speed_director_mode = false
        elseif speed_muted then
          -- un-mute to talking ASI mode
          speed_muted = false
        elseif current.target_speed > 0 and
          current.speed >= speed_director_min_speed and
          current.speed <= speed_director_max_speed then
          -- change to speed director mode if speed is in correct range
          speed_director_mode = true
          at_cruise_speed = true
          speed_muted = false
          uc_alert = ALERT_NONE -- arm the UC alert
          last_report_at = nil
          speed_director_repeat_period = 0
          entering_speed_director_mode(true)
        elseif current.speed >= min_spoken_speed then
          -- otherwise, mute if speed not very low
          speed_muted = true
          play_sound("goodbye")
          disable_dac()
        end
        if not speed_director_mode and not speed_muted then
          -- make next report occur straightaway
          last_report_at = nil
          if not conf.only_speak_when_slow then
            play_sound("airspeed")
            disable_dac()
          end
        end
      end

      button_short_presses = 0
    end

    if general_alert_enabled and general_alert_input_asserted() then
      if general_alert == ALERT_NONE then
        general_alert = ALERT_ACTIVE
        last_alert_at = nil
      end
    elseif general_alert == ALERT_SUPPRESSED then
      general_alert = ALERT_NONE
    end

    if general_alert == ALERT_ACTIVE then
      if not last_alert_at or tmr_getdiffnow(nil, last_alert_at) >= ALERT_PERIOD_MS * 1000 then
        play_sound({"chime", "alert"}, LOUD_VOLUME)
        disable_dac()
        last_alert_at = tmr_read()
        delay_ms(500)
      end
    end


    if speed_director_mode then
      -- if the airspeed has fallen outside of the range of airspeeds
      -- that the speed director supports, revert to talking ASI mode
      if current.speed < speed_director_min_speed or current.speed > speed_director_max_speed then
        speed_director_mode = false
        speed_muted = false
        last_report_at = nil
        if not conf.only_speak_when_slow then
          play_sound("airspeed")
          disable_dac()
        end
        -- we clear short_presses here in case the pilot has just
        -- pressed the tit to exit speed director mode
        ext_switch_short_presses = 0;
      end
    end

    -- calculate a 'virtual speed' which is the current airspeed
    -- adjusted to take into account the change in stall speed due
    -- to the current wing loading - as the G increases, the stall
    -- speed will increase and the virtual speed is reduced by the
    -- same amount - reduced G does not reduce the stall speed below
    -- the 1-G stall speed

    local virtual_speed = floor(current.speed + 0.5)

    if current.target_speed > 0 then
      local g = current.g

      -- with min_g = 1, virtual_speed is not increased when the
      -- measured G is less than unity - much easier to fly because
      -- forward stick movement doesn't trigger immediate "fast"
      -- warnings due to the reduced G
      local min_g = 1
      if g < min_g then
        g = min_g;
        -- clamp to unity until G exceeds G_MAP_LOW and then interpolate
        -- between G_MAP_LOW and G_MAP_HIGH
        if g <= G_MAP_LOW then
          g = 1
        elseif g < G_MAP_HIGH then
          g = 1 + ((G_MAP_HIGH - 1) * (g - G_MAP_LOW) / (G_MAP_HIGH - G_MAP_LOW))
        end
      end

      virtual_speed = virtual_speed - floor(one_g_stall_speed * sqrt(g) - one_g_stall_speed + 0.5) -- round to nearest
    end

    if speed_director_mode then
      local report_now = not last_report_at or tmr_getdiffnow(nil, last_report_at) / 1000 >= sd_repeat_period

      if undercarriage_alert_enabled and not uc_down_asserted() and uc_alert == ALERT_NONE then
        uc_alert = ALERT_ACTIVE
      end

      -- as the speed increases (decreases) and it passes a threshold
      -- from one speed band to another, the threshold is reduced
      -- (increased) by the hysteresis value so that the speed has to
      -- reduce (increase) by a larger amount to return to the
      -- original band - this should reduce the likelyhood of the the
      -- speed oscillating across a threshold and producing a babble
      -- such as "speed good, fast, speed good, fast, speed good ..."

      local VOLUME_DELTA = 5

      if virtual_speed < (insanely_slow_speed + insanely_slow_hysteresis) then
        insanely_slow_hysteresis = hysteresis
        very_slow_hysteresis = hysteresis
        slightly_slow_hysteresis = hysteresis
        if report_now or sd_repeat_period ~= 1000 then
          last_report_at = tmr_read()
          if sd_repeat_period ~= 1000 then
            speed_director_volume = VERY_LOUD_VOLUME
          elseif speed_director_volume >= (QUIET_VOLUME + VOLUME_DELTA) then
            speed_director_volume = speed_director_volume - VOLUME_DELTA
          end
          play_sound({"chime", "veryslow"}, speed_director_volume)
          disable_dac()
          sd_repeat_period = 1000
        end
      elseif virtual_speed < (very_slow_speed + very_slow_hysteresis) then
        insanely_slow_hysteresis = 0
        very_slow_hysteresis = hysteresis
        slightly_slow_hysteresis = hysteresis
        if report_now or sd_repeat_period ~= 2000 then
          last_report_at = tmr_read()
          if sd_repeat_period ~= 2000 then
            speed_director_volume = LOUD_VOLUME
          elseif speed_director_volume >= (QUIET_VOLUME + VOLUME_DELTA) then
            speed_director_volume = speed_director_volume - VOLUME_DELTA
          end
          play_sound("veryslow", speed_director_volume)
          disable_dac()
          sd_repeat_period = 2000
        end
      elseif virtual_speed < (slightly_slow_speed + slightly_slow_hysteresis) then
        insanely_slow_hysteresis = 0
        very_slow_hysteresis = 0
        slightly_slow_hysteresis = hysteresis
        if report_now or sd_repeat_period ~= 4000 then
          last_report_at = tmr_read()
          if sd_repeat_period ~= 4000 then
            speed_director_volume = NORMAL_VOLUME
          elseif speed_director_volume >= (QUIET_VOLUME + VOLUME_DELTA) then
            speed_director_volume = speed_director_volume - VOLUME_DELTA
          end
          play_sound("slow", speed_director_volume)
          disable_dac()
          sd_repeat_period = 4000
        end
      elseif virtual_speed > (insanely_fast_speed - insanely_fast_hysteresis) then
        insanely_fast_hysteresis = hysteresis
        very_fast_hysteresis = hysteresis
        slightly_fast_hysteresis = hysteresis
        if report_now or sd_repeat_period ~= 1000 then
          last_report_at = tmr_read()
          if sd_repeat_period ~= 1000 then
            speed_director_volume = VERY_LOUD_VOLUME
          elseif speed_director_volume >= (QUIET_VOLUME + VOLUME_DELTA) then
            speed_director_volume = speed_director_volume - VOLUME_DELTA
          end
          if not conf.only_speak_when_slow then
            play_sound({"chime", "veryfast"}, speed_director_volume)
            disable_dac()
          end
          sd_repeat_period = 1000
        end
      elseif virtual_speed > (very_fast_speed - very_fast_hysteresis) then
        insanely_fast_hysteresis = 0
        very_fast_hysteresis = hysteresis
        slightly_fast_hysteresis = hysteresis
        if report_now or sd_repeat_period ~= 2000 then
          last_report_at = tmr_read()
          if sd_repeat_period ~= 2000 then
            speed_director_volume = LOUD_VOLUME
          elseif speed_director_volume >= (QUIET_VOLUME + VOLUME_DELTA) then
            speed_director_volume = speed_director_volume - VOLUME_DELTA
          end
          if not conf.only_speak_when_slow then
            play_sound("veryfast", speed_director_volume)
            disable_dac()
          end
          sd_repeat_period = 2000;
        end
      elseif virtual_speed > (slightly_fast_speed - slightly_fast_hysteresis) then
        insanely_fast_hysteresis = 0
        very_fast_hysteresis = 0
        slightly_fast_hysteresis = hysteresis
        if report_now or sd_repeat_period ~= 4000 then
          last_report_at = tmr_read()
          if sd_repeat_period ~= 4000 then
            speed_director_volume = NORMAL_VOLUME
          elseif speed_director_volume >= (QUIET_VOLUME + VOLUME_DELTA) then
            speed_director_volume = speed_director_volume - VOLUME_DELTA
          end
          if not conf.only_speak_when_slow then
            play_sound("fast", speed_director_volume)
            disable_dac()
          end
          sd_repeat_period = 4000
        end
      elseif report_now or sd_repeat_period ~= 8000 then
        insanely_slow_hysteresis = 0
        very_slow_hysteresis = 0
        slightly_slow_hysteresis = 0
        insanely_fast_hysteresis = 0
        very_fast_hysteresis = 0
        slightly_fast_hysteresis = 0
        last_report_at = tmr_read()
        if sd_repeat_period ~= 8000 then
          speed_director_volume = NORMAL_VOLUME
        elseif speed_director_volume >= (VERY_QUIET_VOLUME + (2 * VOLUME_DELTA)) then
          speed_director_volume = speed_director_volume - 2 * VOLUME_DELTA
        end
        if not conf.only_speak_when_slow then
          play_sound("speed", speed_director_volume)
          delay_ms(100)
          play_sound("good", speed_director_volume)
          disable_dac()
        end
        sd_repeat_period = 8000
      end
    else -- not speed_director_mode

      if current.speed >= min_spoken_speed then

        -- if a target speed has been set, detect when into the cruise regime
        if current.target_speed > 0 and current.speed > speed_director_max_speed then
          at_cruise_speed = true
        end

        if at_cruise_speed then
          -- once up to flying speed, arm UC alert
          if uc_alert == ALERT_SUPPRESSED then
            uc_alert = ALERT_NONE
          end

          -- check against virtual speed so as to take G into account
          if virtual_speed < slightly_fast_speed then
            -- (virtual) speed has dropped to near the target speed
            at_cruise_speed = false
            local speed_is_very_slow = virtual_speed < very_slow_speed
            -- if speed is very slow or auto-switching modes, switch now
            -- to speed director
            if conf.auto_switch_mode or speed_is_very_slow then
              last_report_at = nil
              speed_director_mode = true
              speed_muted = false
              if speed_is_very_slow then
                -- suppress verbose intro to speed director mode
                entering_speed_director_mode(false)
              else
                uc_alert = ALERT_NONE -- arm the UC alert
                entering_speed_director_mode(not conf.only_speak_when_slow)
              end
            elseif undercarriage_alert_enabled and not uc_down_asserted() and uc_alert == ALERT_NONE then
              -- trigger UC alert when speed < slightly_fast_speed and UC not down
              uc_alert = ALERT_ACTIVE
              last_alert_at = nil
            end
          end
        end

        if not speed_muted and not speed_director_mode and not conf.only_speak_when_slow then

          -- speak the current speed
          local speed_diff = abs(current.speed - last_spoken_speed)

          if speed_diff >= speed_diff_threshold or
            not last_report_at or
            (tmr_getdiffnow(nil, last_report_at) / 1000 >=
            (conf.repeat_period_multiplier * AIRSPEED_REPEAT_PERIOD_MS)) then

            last_report_at = tmr_read()
            local spoken_speed = floor(current.speed + 0.5)
            say_number(spoken_speed)
            disable_dac()

            if last_spoken_speed == 1 then
              play_sound("cmptoasi")
              disable_dac()
            end

            last_spoken_speed = spoken_speed;
          end
        end
      end
    end

    if current.speed < min_spoken_speed then
      -- disarm UC alert when speed is very low
      uc_alert = ALERT_SUPPRESSED
      -- no longer cruising!
      at_cruise_speed = false
      -- un-mute
      speed_muted = false
    end

    if uc_alert == ALERT_ACTIVE then
      if uc_down_asserted() then
        -- wheels are down now so cancel alert
        uc_alert = ALERT_SUPPRESSED
      elseif not last_alert_at or tmr_getdiffnow(nil, last_alert_at) / 1000 >= ALERT_PERIOD_MS then
        play_sound({"chime", "cwd"}, LOUD_VOLUME)
        disable_dac()
        last_alert_at = tmr_read(nil)
      end
    end

  end -- main loop

end


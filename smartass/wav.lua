
--[[

Simple WAV file player - handles 8 or 16 bit uncompressed audio

Copyright 2014 Smart Avionics Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

--]]

local dac = stm32f4.sa_dac

-- playwavfile(filename, [scaling_percent, [dac_id, [poll_func]]])

local function playwavfile(filename, scaling_percent, dac_id, poll_func)
  scaling_percent = scaling_percent or 100
  dac_id = dac_id or 0
  local f = assert(io.open(filename, "rb"))
  local header = f:read(12)
  if string.sub(header, 1, 4) ~= "RIFF" or string.sub(header, 9, 12) ~= "WAVE" then
    f:close()
    error(string.format("%s is not a WAV file", filename))
  end

  if f:read(4) ~= "fmt " then
    f:close()
    error(string.format("%s header does not contain format chunk", filename))
  end

  header = f:read(20)
  local nextpos1, chunk_size, compression_code, num_channels, sample_rate, dummy1, stride = pack.unpack(header, "<L<H<H<L<L<H");

  --print(string.format("chunk_size = %d, compression_code = %d, num_channels = %d, sample_rate = %d, stride = %d",
  --  chunk_size, compression_code, num_channels, sample_rate, stride))

  if chunk_size > 16 then
    f:read(chunk_size - 16)
  end

  local bytes_per_sample = stride / num_channels

  if compression_code ~= 1 or bytes_per_sample > 2 then
    f:close()
    error(string.format("%s does not contain uncompressed 8 or 16 bit PCM data", filename))
  end

  if f:read(4) ~= "data" then
    f:close()
    error(string.format("%s does not contain a data chunk", filename))
  end

  local nextpos2, data_len = pack.unpack(f:read(4), "<L")

  local read_func = function()
    if poll_func then
      if not poll_func() then
        return nil
      end
    end
    return f:read(1024)
  end

  local num_samp, num_uf = dac.putsamples(dac_id, read_func, sample_rate, bytes_per_sample * 8, num_channels, scaling_percent, 0x8000)
  f:close()
  if num_uf ~= 0 then
    print(string.format("%s: %d samples output, %d underflows", filename, num_samp, num_uf))
  end
end

return {
  playwavfile = playwavfile
}
